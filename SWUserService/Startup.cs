﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Extensions.Logging;
using SWUserService.Models;
using SWUserService.Tasks;
using SWUserService.Utility;
using System;

namespace SWUserService {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            var authConfig = new AuthOptions();
            Configuration.GetSection("AuthOptions").Bind(authConfig);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options => {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters {
                            ValidateIssuer = true,
                            ValidIssuer = authConfig.ISSUER,
                            ValidateAudience = true,
                            ValidAudience = authConfig.AUDIENCE,
                            ValidateLifetime = true,
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            ValidateIssuerSigningKey = true,
                        };
                    });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddEntityFrameworkNpgsql().AddDbContext<SWPlayerDbContext>()
               .BuildServiceProvider();

            services.AddIdentity<User, IdentityRole<Guid>>(config => {
                config.SignIn.RequireConfirmedEmail = true;
                config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                config.Lockout.MaxFailedAccessAttempts = 5;
                config.Lockout.AllowedForNewUsers = true;
                config.User.RequireUniqueEmail = true;
                config.Password.RequireDigit = false;
                config.Password.RequiredLength = 5;
                config.Password.RequireLowercase = false;
                config.Password.RequireUppercase = false;
                config.Password.RequireNonAlphanumeric = false;
            })
            .AddEntityFrameworkStores<SWPlayerDbContext>().AddDefaultTokenProviders();
          
            services.AddSingleton<IEmailSender, EmailSender>();
            
            services.AddCors(options => {
            options.AddPolicy("AllowLocalHost",
                builder => builder.WithOrigins(Configuration.GetValue<string>("AllowedHosts")).AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            });

            services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, DailyRewardUpdater>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseHsts();
            }

            GlobalDiagnosticsContext.Set("configDir", AppDomain.CurrentDomain.BaseDirectory);
            GlobalDiagnosticsContext.Set("connectionString", Configuration.GetConnectionString("DefaultConnection"));

            loggerFactory.AddNLog();

            var serviceProvider = new ServiceCollection()
                                    .AddSingleton(loggerFactory)
                                    .AddLogging();

            app.UseCors("AllowLocalHost");

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
