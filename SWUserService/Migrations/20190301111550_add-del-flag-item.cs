﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWUserService.Migrations {
    public partial class adddelflagitem : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Items",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Items");
        }
    }
}
