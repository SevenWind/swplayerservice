﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWUserService.Attributes;
using SWUserService.Models;
using static SWUserService.Models.Enums;

namespace SWUserService.Controllers {
    [Route("api/v1/[controller]/[action]")]
    [ActivityUpdate]
    [ApiController]
    public class PlayerController : ControllerBase {

        private readonly SWPlayerDbContext db;
        private readonly ILogger<PlayerController> logger;
        private readonly UserManager<User> userManager;

        public PlayerController(IHostingEnvironment env, SWPlayerDbContext _db, ILogger<PlayerController> _loggerFactory, UserManager<User> _userManager) {
            db = _db;
            logger = _loggerFactory;
            userManager = _userManager;
        }

        [ProducesResponseType(200, Type = typeof(DisplayUserProfile))]
        [ProducesResponseType(404)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlayerInfo(Guid id) {
            var user = await db.Users.Include(a => a.playerInfo).Include(a => a.playerInfo.cash).Include(a => a.playerInfo.level).FirstOrDefaultAsync(a => a.Id == id);
            if (user == null)
                return NotFound();
            return Ok(new DisplayUserProfile() {
                name = user.UserName,
                birthday = user.birthDate.ToString("MM/dd/yyyy"),
                level = user.playerInfo.level.level,
                xp = user.playerInfo.xp,
                email = user.Email,
                nextXp = user.playerInfo.level.needXpForNext,
                avatarImage = user.avatarImage,
                avatar_name = user.avatarName
            });
        }

        [ProducesResponseType(200, Type = typeof(ShortPlayerInfo))]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlayerShortInfo(Guid id) {
            try {
                var user = await db.Users.Include(a => a.playerInfo).Include(a => a.playerInfo.cash).Include(a => a.playerInfo.level).FirstOrDefaultAsync(a => a.Id == id);
                if (user == null)
                    return NotFound();
                if(user.LockoutEnabled) {
                    return StatusCode(401, "Этот пользователь заблокирован.");
                }
                user.lastOnlineDate = DateTime.Now;
                await db.SaveChangesAsync();
                var shortInfo = new ShortPlayerInfo() {
                    username = user.UserName,
                    avatar_name = user.avatarName,
                    level = user.playerInfo.level,
                    xp = user.playerInfo.xp,
                    cash = user.playerInfo.cash,
                    avatarImage = user.avatarImage
                };
                return Ok(shortInfo);
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("{id}")]
        public async Task<IActionResult> SavePlayerInfo(Guid id, [FromBody] DisplayUserProfile userData) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var user = db.Users.Find(id);
                    if (user == null)
                        return NotFound();

                    if (user.NormalizedUserName != userData.name.ToUpper() && db.Users.Any(a => a.NormalizedUserName == userData.name.ToUpper()))
                        return BadRequest("Пользователь с таким именем уже существует");
                    if (user.NormalizedEmail != userData.email.ToUpper() && db.Users.Any(a => a.NormalizedEmail == userData.email.ToUpper()))
                        return BadRequest("Пользователь с такой почтой уже существует");

                    user.UserName = userData.name;
                    user.Email = userData.email;
                    user.avatarName = userData.avatar_name;

                    try {
                        user.birthDate = DateTime.ParseExact(userData.birthday, "MM/dd/yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);
                    } catch (Exception e) {
                        user.birthDate = DateTime.ParseExact(userData.birthday, "dd.MM.yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);
                    }
                    if (string.IsNullOrEmpty(userData.avatarImage) == false) {
                        user.avatarImage = userData.avatarImage;
                    }

                    if (userData.password.Length > 0) {
                        var passChangeResult = await userManager.ChangePasswordAsync(user, userData.current_password, userData.password);
                        if (!passChangeResult.Succeeded) {
                            return BadRequest(passChangeResult.Errors);
                        }
                    }

                    await db.SaveChangesAsync();
                    tr.Commit();
                    return Ok();
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(200, Type = typeof(PlayerCash))]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlayerCash(Guid id) {
            try {
                var user = await db.Users.Include(a => a.playerInfo).Include(a => a.playerInfo.cash).FirstOrDefaultAsync(a => a.Id == id);
                if (user == null)
                    return NotFound();
                return Ok(new {
                    user.playerInfo.cash.coins,
                    user.playerInfo.cash.gold
                });
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(200, Type = typeof(Object))]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("pageNumber={pageNumber}&pageSize={pageSize}")]
        public IActionResult GetRating(int pageNumber, int pageSize) {
            try {
                var fullRating = db.Users.Include(a => a.playerInfo).Include(a => a.playerInfo.level);
                var rating = fullRating
                    .OrderByDescending(a => a.playerInfo.level.level).Take(50)
                    .Skip((pageNumber - 1) * pageSize).Take(pageSize)
                    .AsEnumerable()
                    .Select((a, i) => new {
                        position = i + 1,
                        nick = a.UserName,
                        a.playerInfo.level.level,
                        currentXp = a.playerInfo.xp,
                        xpToLvl = a.playerInfo.level.needXpForNext
                    }).ToArray();

                return Ok(new {
                    rating,
                    pageCount = (int)Math.Ceiling((double)fullRating.Count() / pageSize),
                    pageNumber
                });
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(201)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("userId={userId}&value={value}")]
        public async Task<IActionResult> Donate(Guid userId, int value) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var user = await db.Users.Include(a => a.playerInfo).Include(a => a.playerInfo.cash).FirstOrDefaultAsync(a => a.Id == userId);
                    if (user == null)
                        return NotFound();
                    user.playerInfo.cash.gold += value;
                    await db.SaveChangesAsync();
                    tr.Commit();
                    return StatusCode(201);
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Получение адресатов сообщений
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>словарь с ключем ИД юзера и значением - его имя</returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        [HttpGet("{searchString}")]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetMailAddresses(string searchString = "") {
            try {
                var users = db.Users.Where(a => a.NormalizedUserName.StartsWith(searchString.ToUpper())).Select(a => new {
                    id = a.Id,
                    name = a.UserName
                }).ToList();
                //if ("СИСТЕМА".StartsWith(searchString.ToUpper()))
                //users.Add(new {
                //    id = new Guid(),
                //    name = "Система"
                //});
                return Ok(users);
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }
    }
}

