﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWUserService.Attributes;
using SWUserService.Models;
using SWUserService.Utility;
using static SWUserService.Models.Enums;

namespace SWUserService.Controllers {
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class AdminController : ControllerBase {

        private readonly SWPlayerDbContext db;
        private readonly ILogger<AdminController> logger;
        private readonly UserManager<User> userManager;

        public AdminController(SWPlayerDbContext _db, ILogger<AdminController> _loggerFactory, UserManager<User> _userManager) {
            db = _db;
            logger = _loggerFactory;
            userManager = _userManager;
        }

        [ActivityUpdate]
        [AuthorizeRoles(Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("pageNum={pageNum}&pageSize={pageSize}")]
        public async Task<IActionResult> GetUsers(int pageNum, int pageSize, [FromBody] UsersFilter filter) {
            try {
                var users = db.Users
                        .Include(a => a.playerInfo)
                        .Include(a => a.playerInfo.level)
                        .Select(a => new FilteredUser() {
                            id = a.Id,
                            name = a.UserName,
                            lvl = a.playerInfo.level.level,
                            email = a.Email,
                            regDate = a.registryDate,
                            lastOnlineDate = a.lastOnlineDate,
                            isLocked = a.LockoutEnabled
                        });

                users = DbHelpers.FilterUsers(users, filter);
                users = DbHelpers.OrderUsers(users, filter);
                var filteredList = await users.ToListAsync();
                var page = filteredList.Skip((pageNum - 1) * pageSize).Take(pageSize);
                return Ok(new {
                    users = page,
                    pageCount = (int)Math.Ceiling((double)filteredList.Count() / pageSize),
                    pageNum
                });
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ActivityUpdate]
        [AuthorizeRoles(Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("status={status}")]
        public async Task<IActionResult> SetUsersLockStatus(bool status, [FromBody] string[] userIds) {
            try {
                if (userIds.Length == 0) {
                    return BadRequest("Не выбраны пользователи");
                }
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var names = "";
                    var count = userIds.Length;
                    for (int i = 0; i < count; i++) {
                        var user = await db.Users.FindAsync(new Guid(userIds[i]));
                        if (user == null) {
                            return NotFound();
                        }
                        names += user.UserName + ", ";
                        user.LockoutEnabled = status;
                    }
                    names = names.Length > 0 ? names.Remove(names.Length - 2) : names;
                    logger.LogInformation("SetUsersLockStatus: Успешно изменен статус блокировки на " + status + " пользователей: " + names);
                    await db.SaveChangesAsync();
                    tr.Commit();
                    return Ok(true);
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ActivityUpdate]
        [AuthorizeRoles(Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("gold={gold}")]
        public async Task<IActionResult> AddGoldToUsers(int gold, [FromBody] string[] userIds) {
            try {
                if (userIds.Length == 0) {
                    return BadRequest("Не выбраны пользователи");
                }
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var count = userIds.Length;
                    var names = "";
                    for (int i = 0; i < count; i++) {
                        var user = await db.Users.Include(a=>a.playerInfo).Include(a => a.playerInfo.cash).FirstOrDefaultAsync(a => a.Id == new Guid(userIds[i]));
                        if (user == null) {
                            return NotFound();
                        }
                        names += user.UserName + ", ";
                        user.playerInfo.cash.gold += gold;
                    }
                    names = names.Length > 0 ? names.Remove(names.Length - 2) : names;
                    logger.LogInformation("AddGoldToUsers: Золото в размере " + gold + " успешно начислено пользователям: " + names);
                    await db.SaveChangesAsync();
                    tr.Commit();
                    return Ok(true);
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

    }
}
