﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWUserService.Attributes;
using SWUserService.Models;
using SWUserService.Utility;
using static SWUserService.Models.Enums;

namespace SWUserService.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    [ActivityUpdate]
    [ApiController]
    public class CollectionController : ControllerBase {

        private readonly SWPlayerDbContext db;
        private readonly ILogger<CollectionController> logger;

        public CollectionController(SWPlayerDbContext _db, ILogger<CollectionController> _logger) {
            db = _db;
            logger = _logger;
        }

        [HttpGet("{userId}")]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetUserCollection(Guid userId) {
            try {
                var user = await db.Users
                    .Include(a => a.playerInfo)
                    .Include(a => a.playerInfo.collection)
                    .FirstOrDefaultAsync(a => a.Id == userId);
                if (user == null)
                    return NotFound();

                return Ok(JsonHelpers.SerializeObjectWithLoopIgnore(user.playerInfo.collection));
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }
    }
}