﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWUserService.Attributes;
using SWUserService.Models;
using SWUserService.Utility;
using static SWUserService.Models.Enums;

namespace SWUserService.Controllers {
    [Route("api/v1/[controller]/[action]")]
    [ActivityUpdate]
    [ApiController]
    public class CalendarController : ControllerBase {

        private readonly SWPlayerDbContext db;
        private readonly ILogger<CalendarController> logger;

        public CalendarController(SWPlayerDbContext _db, ILogger<CalendarController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserCalendar(Guid userId) {
            try {
                var user = await db.Users
                    .Include(a => a.playerInfo)
                    .Include(a => a.playerInfo.days)
                    .FirstOrDefaultAsync(a => a.Id == userId);
                if (user == null)
                    return NotFound();

                return Ok(new {
                    days = user.playerInfo.days.OrderBy(a => a.day).Select(a => new {
                        a.day,
                        a.dayChecked
                    })
                });
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserCalendarRewardStatuses(Guid userId) {
            try {
                var user = await db.Users
                    .Include(a => a.playerInfo)
                    .Include(a => a.playerInfo.calendarRewards)
                    .FirstOrDefaultAsync(a => a.Id == userId);
                if (user == null)
                    return NotFound();

                return Ok(new {
                    days = user.playerInfo.calendarRewards.OrderBy(a => a.id).Select(a => new {
                        a.id,
                        a.code,
                        a.rewardCollected
                    })
                });
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(500)]
        [HttpPost("userId={userId}&dayNumber={dayNumber}")]
        public async Task<IActionResult> CheckDay(Guid userId, int dayNumber, [FromBody] BuyItem itemData) {
            try {
                if (DateTime.Now.Day < dayNumber)
                    return BadRequest();

                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var user = await db.Users
                        .Include(a => a.playerInfo)
                        .Include(a => a.playerInfo.items)
                        .Include(a => a.playerInfo.cash)
                        .Include(a => a.playerInfo.days)
                        .FirstOrDefaultAsync(a => a.Id == userId);
                    if (user == null)
                        return NotFound();

                    var day = user.playerInfo.days.FirstOrDefault(a => a.day == dayNumber);
                    if (day == null)
                        return NotFound();

                    if (day.dayChecked) {
                        return BadRequest();
                    }

                    var item = await db.Items.FirstOrDefaultAsync(a => a.id == itemData.itemId);
                    if (item == null)
                        return NotFound();
                    if (item.isDeleted)
                        return BadRequest("Предмет удален");

                    //Снимаем с кошелька нужную сумму, если это возможно
                    try {
                        CashHelpers.PayMoney(user, itemData.currency, itemData.cost);
                    } catch (Exception e) {
                        return NotFound(e.Message);
                    }

                    //Если это монеты - добавлеяем их в кошелек
                    if (item.code == "Coins") {
                        user.playerInfo.cash.coins += itemData.count;
                    } else if (item.code == "Gold") {
                        user.playerInfo.cash.gold += itemData.count;
                    } else {
                        //иначе в инвентарь
                        var inventoryItem = user.playerInfo.items.FirstOrDefault(a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                        if (inventoryItem == null) {
                            user.playerInfo.items.Add(new InventoryItems() {
                                count = itemData.count,
                                item_id = item.id
                            });
                        } else {
                            inventoryItem.count += itemData.count;
                        }
                    }

                    day.dayChecked = true;

                    await db.SaveChangesAsync();
                    tr.Commit();
                }
                return Ok();
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("{userId}")]
        public async Task<IActionResult> CollectReward(Guid userId, [FromBody] CalendarRewardExternal rewardInfo) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var user = await db.Users
                        .Include(a => a.playerInfo)
                        .Include(a => a.playerInfo.items)
                        .Include(a => a.playerInfo.cash)
                        .Include(a => a.playerInfo.calendarRewards)
                        .Include(a => a.playerInfo.days)
                        .FirstOrDefaultAsync(a => a.Id == userId);
                    if (user == null)
                        return NotFound();

                    var reward = user.playerInfo.calendarRewards.FirstOrDefault(a => a.code == rewardInfo.code);
                    if (reward == null)
                        return NotFound("Тип награды не найден");
                    if (reward.rewardCollected)
                        return BadRequest("Награда уже собрана");
                    if (rewardInfo.id != 1 && user.playerInfo.days.Count(a => a.dayChecked) < rewardInfo.needCheckDaysCount) {
                        return BadRequest("В календаре недостаточно отмечено дней");
                    }

                    foreach (var itemData in rewardInfo.items) {
                        var item = await db.Items.FirstOrDefaultAsync(a => a.id == itemData.itemId);
                        if (item == null) {
                            logger.LogError("Не найден предмет входящий в награду " + rewardInfo.code + " с ID = " + itemData.itemId);
                            return NotFound("Не найден предмет входящий в награду");
                        }
                        if (item.isDeleted)
                            return BadRequest("Предмет удален");
                        //Если это монеты - добавлеяем их в кошелек
                        if (item.code == "Coins") {
                            user.playerInfo.cash.coins += itemData.count;
                        } else if (item.code == "Gold") {
                            user.playerInfo.cash.gold += itemData.count;
                        } else {
                            //иначе в инвентарь
                            var inventoryItem = user.playerInfo.items.FirstOrDefault(a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                            if (inventoryItem == null) {
                                user.playerInfo.items.Add(new InventoryItems() {
                                    count = itemData.count,
                                    item_id = item.id
                                });
                            } else {
                                inventoryItem.count += itemData.count;
                            }
                        }
                    }

                    reward.rewardCollected = true;

                    await db.SaveChangesAsync();
                    tr.Commit();
                }
                return Ok();
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

    }
}