﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using SWUserService.Attributes;
using SWUserService.Models;
using SWUserService.Utility;
using System;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using static SWUserService.Models.Enums;

namespace SWUserService.Controllers {
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase {

        private readonly SWPlayerDbContext db;
        private readonly ILogger<AuthController> logger;
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly IEmailSender emailSender;
        private readonly IConfiguration configuration;

        public AuthController(SWPlayerDbContext _db, ILogger<AuthController> _loggerFactory,
            UserManager<User> _userManager, SignInManager<User> _signInManager,
            IEmailSender _emailSender, IConfiguration _configuration) {
            db = _db;
            logger = _loggerFactory;
            userManager = _userManager;
            signInManager = _signInManager;
            emailSender = _emailSender;
            configuration = _configuration;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SignIn([FromBody] AuthForm form) {
            try {
                var user = await userManager.FindByEmailAsync(form.email);
                if (user == null) {
                    return NotFound();
                }
                if (user.LockoutEnabled) {
                    return BadRequest("Пользователь заблокирован");
                }

                var result = await signInManager.PasswordSignInAsync(user.UserName, form.password, form.rememberMe, lockoutOnFailure: false);
                if (result.Succeeded) {
                    var userRoles = await userManager.GetRolesAsync(user);

                    user.lastOnlineDate = DateTime.Now;
                    await db.SaveChangesAsync();

                    var claims = TokenHelpers.GetIdentityClaims(user, userRoles.First());

                    var authConfig = new AuthOptions();
                    configuration.GetSection("AuthOptions").Bind(authConfig);
                    var now = DateTime.UtcNow;

                    var jwt = new JwtSecurityToken(
                            issuer: authConfig.ISSUER,
                            audience: authConfig.AUDIENCE,
                            notBefore: now,
                            claims: claims,
                            expires: now.Add(TimeSpan.FromMinutes(authConfig.LIFETIME)),
                            signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                    return Ok(new {
                        access_token = encodedJwt,
                        user_roles = userRoles,
                        user_id = user.Id,
                        user_name = user.UserName
                    });
                } else {
                    if (result.IsNotAllowed) {
                        return BadRequest("Почтовый адрес не подтвержден");
                    }
                    if (result.IsLockedOut) {
                        return BadRequest("Пользователь заблокирован");
                    }
                    return BadRequest("Не верный логин или пароль");
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [ActivityUpdate]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<IActionResult> SignOut() {
            try {
                await signInManager.SignOutAsync();
                return Ok();
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegForm form) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    if (string.IsNullOrWhiteSpace(form.email) || string.IsNullOrWhiteSpace(form.password)) {
                        return BadRequest("Не заполнены почта или пароль");
                    }

                    if (form.password != form.confirmPassword) {
                        return BadRequest("Пароли не совпадают");
                    }

                    var user = await userManager.FindByNameAsync(form.name);
                    if (user != null) {
                        return BadRequest("Пользователь с таким именем уже существует");
                    }
                    user = await userManager.FindByEmailAsync(form.email);
                    if (user != null) {
                        return BadRequest("Пользователь с такой почтой уже существует");
                    }

                    var playerCash = new PlayerCash() {
                        id = Guid.NewGuid(),
                        coins = 100000,
                        gold = 100
                    };

                    await db.PlayerCashes.AddAsync(playerCash);

                    var playerInfo = new PlayerInfo() {
                        id = Guid.NewGuid(),
                        level_id = 1,
                        xp = 0,
                        cash_id = playerCash.id,
                    };

                    await db.PlayerInfos.AddAsync(playerInfo);

                    await DbHelpers.CreateCalendar(db, playerInfo.id);

                    var newUser = new User {
                        Id = Guid.NewGuid(),
                        UserName = form.name,
                        Email = form.email,
                        birthDate = form.birthdate,
                        registryDate = DateTime.Now,
                        avatarImage = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", "anonim-avatar.png")))),
                        NormalizedEmail = form.email.ToUpper(),
                        NormalizedUserName = form.name.ToUpper(),
                        SecurityStamp = Guid.NewGuid().ToString(),
                        playerInfo_id = playerInfo.id,
                        EmailConfirmed = true // По возможности разобраться с подтвреждением
                    };
                    newUser.PasswordHash = userManager.PasswordHasher.HashPassword(newUser, form.password);
                    await db.Users.AddAsync(newUser);

                    await db.UserRoles.AddAsync(new IdentityUserRole<Guid>() {
                        UserId = newUser.Id,
                        RoleId = db.Roles.First(a => a.NormalizedName == "PLAYER").Id
                    });

                    await db.SaveChangesAsync();

                    //var emailConfirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(newUser);
                    //var tokenVerificationUrl = Url.Action(
                    //    "VerifyEmail", "Auth",
                    //    new {
                    //        Id = newUser.Id,
                    //        token = emailConfirmationToken
                    //    },
                    //    Request.Scheme);

                    //await emailSender.SendEmailAsync(form.email, "Пожалуйста, подтвердите почту", $"Нажмите <a href=\"{HtmlEncoder.Default.Encode(tokenVerificationUrl)}\">тут</a> для подтверждения почты.");

                    tr.Commit();
                    return Ok();
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("{email}")]
        public async Task<IActionResult> ReSendEmailConfirm(string email) {
            try {
                var user = await userManager.FindByEmailAsync(email);
                if (user == null)
                    return NotFound();
                if (user.EmailConfirmed)
                    return NotFound();

                var emailConfirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(user);
                var tokenVerificationUrl = Url.Action(
                    "VerifyEmail", "Auth",
                    new {
                        Id = user.Id,
                        token = emailConfirmationToken
                    },
                    Request.Scheme);

                await emailSender.SendEmailAsync(email, "Пожалуйста, подтвердите почту", $"Нажмите <a href=\"{HtmlEncoder.Default.Encode(tokenVerificationUrl)}\">тут</a> для подтверждения почты.");

                return Ok();
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        [AllowAnonymous]
        [HttpGet("id={id}&token={token}")]
        public async Task<IActionResult> VerifyEmail(string id, string token) {
            try {
                var user = await userManager.FindByIdAsync(id);
                if (user == null)
                    throw new InvalidOperationException();

                var emailConfirmationResult = await userManager.ConfirmEmailAsync(user, token);
                if (!emailConfirmationResult.Succeeded) {
                    return new RedirectResult("http://localhost:4200/signup");
                }

                return new RedirectResult("http://localhost:4200/user");
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return new RedirectResult("http://localhost:4200/signup");
            }
        }

        [AllowAnonymous]
        [HttpPost("{email}")]
        public async Task<IActionResult> ForgotPassword(string email) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var user = await userManager.FindByEmailAsync(email);
                    if (user == null)
                        return NotFound();
                    var newPassword = IdentityHelper.GenerateRandomPassword();
                    user.PasswordHash = userManager.PasswordHasher.HashPassword(user, newPassword);
                    await db.SaveChangesAsync();
                    await emailSender.SendEmailAsync(email, "Пароль был сброшен!", "Новый пароль для входа в систему: " + newPassword + " Смените его как можно скорее.");
                    tr.Commit();
                    return Ok();
                }
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }
    }
}