﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWUserService.Attributes;
using SWUserService.Models;
using SWUserService.Utility;
using static SWUserService.Models.Enums;

namespace SWUserService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [ActivityUpdate]
    [ApiController]
    public class InventoryController : ControllerBase {

        private readonly SWPlayerDbContext db;
        private readonly ILogger<InventoryController> logger;

        public InventoryController (SWPlayerDbContext _db, ILogger<InventoryController> _logger) {
            db = _db;
            logger = _logger;
        }

        [ProducesResponseType (200)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet ("userId={userId}&searchString={searchString}&pageNumber={pageNumber}&pageSize={pageSize}")]
        public async Task<IActionResult> GetInventoryItems (Guid userId, int pageNumber, int pageSize, string searchString) {
            try {
                var user = await db.Users
                    .Include (a => a.playerInfo)
                    .Include (a => a.playerInfo.cash)
                    .Include (a => a.playerInfo.items)
                    .FirstOrDefaultAsync (a => a.Id == userId);
                if (user == null)
                    return NotFound ();

                var inventoryItems = user.playerInfo.items;

                //Подгрузка необходимых зависимостей
                var inventoryItemsCount = inventoryItems.Count;
                for (int j = 0; j < inventoryItemsCount; j++) {
                    db.Entry (inventoryItems[j]).Reference (a => a.item).Load ();
                    db.Entry (inventoryItems[j].item).Reference (a => a.rarity).Load ();
                }

                if (searchString.Length > 0 && searchString != "_") {
                    var filteredList = inventoryItems.Where (a => a.item.name.ToLower ().StartsWith (searchString.ToLower ()));
                    inventoryItems = filteredList.ToList ();
                }

                var needItems = inventoryItems
                    .OrderByDescending (a => a.item.rarity.id)
                    .Skip ((pageNumber - 1) * pageSize).Take (pageSize)
                    .AsEnumerable ()
                    .Select (a => new {
                        item = new Item () {
                                id = a.item.id,
                                    code = a.item.code,
                                    name = a.item.name,
                                    image = a.item.image,
                                    rarity = a.item.rarity
                            },
                            a.count,
                    }).ToArray ();

                return Ok (new {
                    items = needItems,
                        pageCount = (int) Math.Ceiling ((double) inventoryItems.Count () / pageSize),
                        pageNumber
                });
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [ProducesResponseType (201)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("{userId}")]
        public async Task<IActionResult> BuyItem (Guid userId, [FromBody] BuyItem itemData) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var user = await db.Users
                        .Include (a => a.playerInfo)
                        .Include (a => a.playerInfo.cash)
                        .Include (a => a.playerInfo.items)
                        .Include (a => a.playerInfo.collection)
                        .FirstOrDefaultAsync (a => a.Id == userId);
                    if (user == null)
                        return NotFound ();
                    var item = await db.Items.FirstOrDefaultAsync (a => a.id == itemData.itemId);
                    if (item == null)
                        return NotFound ();
                    if (item.isDeleted)
                        return BadRequest ("Предмет удален");
                    var calculatedCost = itemData.cost * itemData.count;
                    //Снимаем с кошелька нужную сумму, если это возможно
                    try {
                        if (item.code == "Coins" || item.code == "Gold") {
                            // Т.к. эти валюты исчесляются десятками и сотнями тысяч - не нужно умножать количество на стоимость
                            CashHelpers.PayMoney (user, itemData.currency, itemData.cost);
                        } else {
                            CashHelpers.PayMoney (user, itemData.currency, calculatedCost);
                        }
                    } catch (Exception e) {
                        return NotFound (e.Message);
                    }

                    //Если это монеты - добавлеяем их в кошелек
                    if (item.code == "Coins") {
                        user.playerInfo.cash.coins += itemData.count;
                        calculatedCost = itemData.cost;
                    } else if (item.code == "Gold") {
                        user.playerInfo.cash.gold += itemData.count;
                        calculatedCost = itemData.cost;
                        // Если это фрагменты персонажей - добавляем их в коллекцию
                    } else if (item.code.StartsWith (ItemPrefix.CHARACTER_PREFIX)) {
                        var collItem = user.playerInfo.collection.FirstOrDefault (a => a.character_id == item.characterId);
                        if (collItem == null) {
                            collItem = (new PlayerCollectionItem () {
                                character_id = item.characterId,
                                    starCount = 0,
                                    fragmentCount = itemData.count
                            });
                            user.playerInfo.collection.Add (collItem);
                        } else {
                            collItem.fragmentCount += itemData.count;
                        }
                        var star = db.StarFragments.Find (collItem.starCount + 1);
                        if (star != null) {
                            if (collItem.fragmentCount >= star.needFragments) {
                                collItem.fragmentCount -= star.needFragments;
                                collItem.starCount++;
                            }
                        }
                    } else {
                        //иначе в инвентарь
                        var inventoryItem = user.playerInfo.items.FirstOrDefault (a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                        if (inventoryItem == null) {
                            user.playerInfo.items.Add (new InventoryItems () {
                                count = itemData.count,
                                    item_id = item.id
                            });
                        } else {
                            inventoryItem.count += itemData.count;
                        }
                    }

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                }
                return StatusCode (201);
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [ProducesResponseType (201)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("{userId}")]
        public async Task<IActionResult> SellItem (Guid userId, [FromBody] BuyItem itemData) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var user = await db.Users
                        .Include (a => a.playerInfo)
                        .Include (a => a.playerInfo.cash)
                        .Include (a => a.playerInfo.items)
                        .FirstOrDefaultAsync (a => a.Id == userId);
                    if (user == null)
                        return NotFound ();
                    var item = await db.Items.FirstOrDefaultAsync (a => a.id == itemData.itemId);
                    if (item == null)
                        return NotFound ();

                    //Ищем предмет
                    var inventoryItem = user.playerInfo.items.FirstOrDefault (a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                    if (inventoryItem == null)
                        return NotFound ("Item not in inventory");

                    //Удаляем предмет, если его количество меньше чем пришедшее колличество
                    if (inventoryItem.count <= itemData.count) {
                        //Обрезаем количество проданных предметов, если пришло больше, чем есть
                        itemData.count = inventoryItem.count;
                        user.playerInfo.items.Remove (inventoryItem);
                    } else {
                        inventoryItem.count -= itemData.count;
                    }

                    var calculatedCost = itemData.cost * itemData.count;
                    //Начисляем валюту
                    if (itemData.currency == "GOLD") {
                        user.playerInfo.cash.gold += calculatedCost;
                    } else {
                        user.playerInfo.cash.coins += calculatedCost;
                    }

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                }
                return StatusCode (201);
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [ProducesResponseType (201)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        [HttpPost ("userId={userId}&xp={xp}")]
        public async Task<IActionResult> AddXp (Guid userId, int xp) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var user = await db.Users
                        .Include (a => a.playerInfo)
                        .Include (a => a.playerInfo.level)
                        .FirstOrDefaultAsync (a => a.Id == userId);
                    if (user == null)
                        return NotFound ();

                    await DbHelpers.AddXpToPlayer (db, user, xp);
                    tr.Commit ();
                    return StatusCode (201, new {
                        user.playerInfo.level.level,
                            user.playerInfo.xp,
                            xpToNextLvl = user.playerInfo.level.needXpForNext
                    });
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [ProducesResponseType (201)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("userId={userId}&fromTreasure={fromTreasure}")]
        public async Task<IActionResult> AddItemsToInventory (Guid userId, bool fromTreasure, [FromBody] BuyItem[] itemData) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var user = await db.Users
                        .Include (a => a.playerInfo)
                        .Include (a => a.playerInfo.collection)
                        .Include (a => a.playerInfo.cash)
                        .Include (a => a.playerInfo.level)
                        .Include (a => a.playerInfo.items)
                        .FirstOrDefaultAsync (a => a.Id == userId);
                    if (user == null)
                        return NotFound ();
                    var itemsCount = itemData.Length;
                    for (int i = 0; i < itemsCount; i++) {
                        var item = await db.Items.FirstOrDefaultAsync (a => a.id == itemData[i].itemId);
                        if (item == null)
                            return NotFound ();
                        if (item.isDeleted)
                            return BadRequest ("Предмет удален");
                        // добавляем опыт за получение каждого предмета
                        if (fromTreasure) {
                            await DbHelpers.AddXpToPlayer (db, user, itemData[i].xp);
                        }

                        //Если это монеты - добавлеяем их в кошелек
                        if (item.code == "Coins") {
                            user.playerInfo.cash.coins += itemData[i].count;
                        } else if (item.code == "Gold") {
                            user.playerInfo.cash.gold += itemData[i].count;
                        }
                        // Если это фрагменты персонажей - добавляем их в коллекцию
                        else if (item.code.StartsWith (ItemPrefix.CHARACTER_PREFIX)) {
                            var collItem = user.playerInfo.collection.FirstOrDefault (a => a.character_id == item.characterId);
                            if (collItem == null) {
                                collItem = (new PlayerCollectionItem () {
                                    character_id = item.characterId,
                                        starCount = 0,
                                        fragmentCount = itemData[i].count
                                });
                                user.playerInfo.collection.Add (collItem);
                            } else {
                                collItem.fragmentCount += itemData[i].count;
                            }
                            var star = db.StarFragments.Find (collItem.starCount + 1);
                            if (star != null) {
                                if (collItem.fragmentCount >= star.needFragments) {
                                    collItem.fragmentCount -= star.needFragments;
                                    collItem.starCount++;
                                }
                            }
                        } else {
                            //иначе в инвентарь
                            var inventoryItem = user.playerInfo.items.FirstOrDefault (a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                            if (inventoryItem == null) {
                                user.playerInfo.items.Add (new InventoryItems () {
                                    count = itemData[i].count,
                                        item_id = item.id
                                });
                            } else {
                                inventoryItem.count += itemData[i].count;
                            }
                        }
                    }

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                }
                return StatusCode (201, true);
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Открытие сундука
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="count">количество открытий сундука</param>
        /// <param name="itemDatas">ресурсы для открытия 1го сундука</param>
        [HttpPost ("userId={userId}&count={count}")]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> CanOpenTreasure (Guid userId, int count, [FromBody] BuyItem[] itemDatas) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var user = await db.Users
                        .Include (a => a.playerInfo)
                        .Include (a => a.playerInfo.cash)
                        .Include (a => a.playerInfo.items)
                        .FirstOrDefaultAsync (a => a.Id == userId);
                    if (user == null)
                        return NotFound ();
                    var itemsCount = itemDatas.Length;
                    for (int c = 0; c < count; c++) {

                        for (int i = 0; i < itemsCount; i++) {
                            var item = await db.Items.FirstOrDefaultAsync (a => a.id == itemDatas[i].itemId);
                            if (item == null)
                                return NotFound ();
                            if (item.isDeleted)
                                return BadRequest ("Предмет удален");
                            //Снимаем с кошелька нужную сумму, если это возможно
                            try {
                                if (item.code == "Coins" || item.code == "Gold") {
                                    // Т.к. эти валюты исчесляются десятками и сотнями тысяч - не нужно умножать количество на стоимость
                                    CashHelpers.PayMoney (user, itemDatas[i].currency, itemDatas[i].count);
                                } else {
                                    //Ищем предмет
                                    var inventoryItem = user.playerInfo.items.FirstOrDefault (a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                                    if (inventoryItem == null)
                                        return NotFound ("В инвентаре недостаточное количество предмета: \"" + item.name + "\"");

                                    if (inventoryItem.count < itemDatas[i].count) {
                                        //В инвентаре недостаточно предметов
                                        return NotFound ("В инвентаре недостаточное количество предмета: \"" + item.name + "\"");
                                    } else {
                                        inventoryItem.count -= itemDatas[i].count;
                                    }
                                    if (inventoryItem.count == 0) {
                                        db.InventoryItems.Remove (inventoryItem);
                                    }
                                }
                            } catch (Exception e) {
                                return NotFound (e.Message);
                            }
                        }
                    }
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Возварат ресурсов за неудачное открытие сундука
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="count">количество открытий сундука</param>
        /// <param name="itemDatas">ресурсы для открытия 1го сундука</param>
        [HttpPost ("userId={userId}&count={count}")]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ReturnTreasureResources (Guid userId, int count, [FromBody] BuyItem[] itemData) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var user = await db.Users
                        .Include (a => a.playerInfo)
                        .Include (a => a.playerInfo.cash)
                        .Include (a => a.playerInfo.items)
                        .FirstOrDefaultAsync (a => a.Id == userId);
                    if (user == null)
                        return NotFound ();
                    var itemsCount = itemData.Length;
                    for (int c = 0; c < count; c++) {

                        for (int i = 0; i < itemsCount; i++) {
                            var item = await db.Items.FirstOrDefaultAsync (a => a.id == itemData[i].itemId);
                            if (item == null)
                                return NotFound ();
                            if (item.isDeleted)
                                return BadRequest ("Предмет удален");
                            //возвращаем предметы и деньги
                            if (item.code == "Gold") {
                                user.playerInfo.cash.gold += itemData[i].count;
                            } else if (item.code == "Coins") {
                                user.playerInfo.cash.coins += itemData[i].count;
                            } else {
                                //иначе в инвентарь
                                var inventoryItem = user.playerInfo.items.FirstOrDefault (a => a.item_id == item.id && a.player_id == user.playerInfo.id);
                                if (inventoryItem == null) {
                                    user.playerInfo.items.Add (new InventoryItems () {
                                        count = itemData[i].count,
                                            item_id = item.id
                                    });
                                } else {
                                    inventoryItem.count += itemData[i].count;
                                }
                            }
                        }
                    }
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("{itemId}")]
        public async Task<IActionResult> DeleteItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = db.Items.Find (itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isDeleted = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost ("{itemId}")]
        public async Task<IActionResult> RestoreItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = db.Items.Find (itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isDeleted = false;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        public async Task<IActionResult> SaveItem ([FromBody] Item item) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbItem = await db.Items.FirstOrDefaultAsync (a => a.id == item.id);
                    if (dbItem == null) {
                        // create new
                        dbItem = new Item ();
                        dbItem.id = item.id;
                        dbItem.name = item.name;
                        dbItem.code = item.code;
                        dbItem.image = item.image;
                        dbItem.rarity_id = item.rarity_id;
                        dbItem.characterId = item.characterId;
                        await db.Items.AddAsync (dbItem);
                        await db.SaveChangesAsync ();
                        tr.Commit ();
                        return Ok ();
                    }
                    // update
                    dbItem.name = item.name;
                    dbItem.code = item.code;
                    if (!string.IsNullOrWhiteSpace (item.image) && item.image != dbItem.image)
                        dbItem.image = item.image;
                    dbItem.rarity_id = item.rarity_id;
                    dbItem.characterId = item.characterId;

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}