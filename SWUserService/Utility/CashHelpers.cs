﻿using SWUserService.Models;
using System;

namespace SWUserService.Utility {
    public class CashHelpers {

        /// <summary>
        /// Оплата покупки
        /// </summary>
        /// <param name="user">пользователь</param>
        /// <param name="currency">валюта</param>
        /// <param name="cost">цена</param>
        public static void PayMoney(User user, string currency, int cost) {
            //Снимаем с кошелька нужную сумму, если это возможно
            if (currency == "GOLD") {
                if (user.playerInfo.cash.gold < cost) {
                    throw new Exception("Недостаточно золота");
                } else {
                    user.playerInfo.cash.gold -= cost;
                }
            } else {
                if (user.playerInfo.cash.coins < cost) {
                    throw new Exception("Недостаточно монет");
                } else {
                    user.playerInfo.cash.coins -= cost;
                }
            }
        }
    }
}
