﻿using Microsoft.AspNetCore.Identity;
using SWUserService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SWUserService.Utility {
    public static class TokenHelpers {

        public static List<Claim> GetIdentityClaims(User user, string role) {
            try {

                IdentityOptions options = new IdentityOptions();
                var claims = new List<Claim> {
                        new Claim(options.ClaimsIdentity.UserNameClaimType, user.UserName),
                        new Claim(options.ClaimsIdentity.RoleClaimType, role),
                        new Claim(options.ClaimsIdentity.UserIdClaimType, user.Id.ToString())
                    };
                return claims;
            } catch (Exception e) {
                return null;
            }
        }
    }
}
