﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SWUserService.Models;
using static SWUserService.Models.Enums;

namespace SWUserService.Utility {
    public static class DbHelpers {
        /// <summary>
        /// Начисление опыта игроку (нужно оборачивать в транзакцию)
        /// </summary>
        /// <param name="db">констекст бд</param>
        /// <param name="user">пользователь</param>
        /// <param name="xp">копличество опыта</param>
        public static async Task AddXpToPlayer (SWPlayerDbContext db, User user, int xp) {
            user.playerInfo.xp += xp;
            if (user.playerInfo.xp >= user.playerInfo.level.needXpForNext) {
                user.playerInfo.xp -= user.playerInfo.level.needXpForNext;
                user.playerInfo.level_id++;
            }
            await db.SaveChangesAsync ();
        }

        /// <summary>
        /// Заполнение календаря нового пользователя (нужно оборачивать в транзакцию)
        /// </summary>
        /// <param name="db">констекст бд</param>
        /// <param name="playerInfoId">ид игрока</param>
        public static async Task CreateCalendar (SWPlayerDbContext db, Guid playerInfoId) {
            var calendarDays = new List<CalendarDay> () {
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 1,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 2,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 3,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 4,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 5,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 6,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 7,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 8,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 9,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 10,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 11,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 12,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 13,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 14,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 15,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 16,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 17,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 18,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 19,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 20,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 21,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 22,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 23,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 24,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 25,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 26,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 27,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 28,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 29,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 30,
                playerInfo_id = playerInfoId,
                },
                new CalendarDay () {
                id = Guid.NewGuid (),
                day = 31,
                playerInfo_id = playerInfoId,
                }
            };
            var rewards = new List<CalendarReward> () {
                new CalendarReward () {
                code = "DAILY",
                playerInfo_id = playerInfoId,
                },
                new CalendarReward () {
                code = "7_DAYS",
                playerInfo_id = playerInfoId,
                },
                new CalendarReward () {
                code = "14_DAYS",
                playerInfo_id = playerInfoId,
                },
                new CalendarReward () {
                code = "21_DAYS",
                playerInfo_id = playerInfoId,
                },
                new CalendarReward () {
                code = "FULL_MONTH",
                playerInfo_id = playerInfoId,
                }
            };

            await db.CalendarDays.AddRangeAsync (calendarDays);
            await db.CalendarRewards.AddRangeAsync (rewards);
        }

        /// <summary>
        /// Фильтрация пользователей
        /// </summary>
        /// <param name="users">пользователи</param>
        /// <param name="filter">фильтр</param>
        public static IQueryable<FilteredUser> FilterUsers (IQueryable<FilteredUser> users, UsersFilter filter) {
            if (!string.IsNullOrEmpty (filter.name)) {
                users = users.Where (a => a.name.ToLower ().StartsWith (filter.name.ToLower ()));
            }
            if (!string.IsNullOrEmpty (filter.email)) {
                users = users.Where (a => a.email.ToLower ().StartsWith (filter.email.ToLower ()));
            }
            if (filter.fromLvl != 0) {
                if (filter.toLvl != 0) {
                    users = users.Where (a => a.lvl >= filter.fromLvl && a.lvl <= filter.toLvl);
                } else {
                    users = users.Where (a => a.lvl >= filter.fromLvl);
                }
            } else {
                if (filter.toLvl != 0) {
                    users = users.Where (a => a.lvl <= filter.toLvl);
                }
            }
            try {
                if (!string.IsNullOrEmpty (filter.fromRegDate)) {
                    DateTime fromDate = DateTime.ParseExact (filter.fromRegDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (!string.IsNullOrEmpty (filter.toRegDate)) {
                        DateTime toDate = DateTime.ParseExact (filter.fromRegDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        users = users.Where (a => a.regDate >= fromDate && a.regDate <= toDate);
                    } else {
                        users = users.Where (a => a.regDate >= fromDate);
                    }
                } else {
                    if (!string.IsNullOrEmpty (filter.toRegDate)) {
                        DateTime toDate = DateTime.ParseExact (filter.fromRegDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        users = users.Where (a => a.regDate <= toDate);
                    }
                }
            } catch (Exception e) {

            }

            if ((int) filter.status != -1) {
                switch ((UserStatus) filter.status) {
                    case UserStatus.В_сети:
                        users = users.Where (a => (DateTime.Now - a.lastOnlineDate).Minutes <= 5);
                        break;
                    case UserStatus.Недавно_был_в_сети:
                        users = users.Where (a => ((DateTime.Now - a.lastOnlineDate).Minutes > 5) && (DateTime.Now - a.lastOnlineDate).Minutes <= 15);
                        break;
                    case UserStatus.Не_в_сети:
                        users = users.Where (a => ((DateTime.Now - a.lastOnlineDate).Minutes > 5) && (DateTime.Now - a.lastOnlineDate).Minutes <= 60);
                        break;
                    case UserStatus.Заблокирован:
                        users = users.Where (a => a.isLocked);
                        break;
                    default:
                        break;
                }
            }

            return users;
        }

        /// <summary>
        /// Сортировка пользователей
        /// </summary>
        /// <param name="users">пользователи</param>
        /// <param name="filter">фильтр</param>
        public static IQueryable<FilteredUser> OrderUsers (IQueryable<FilteredUser> users, UsersFilter filter) {
            if ((SortDirection) filter.nameSort != SortDirection.NONE) {
                if ((SortDirection) filter.nameSort == SortDirection.ASC) {
                    return users.OrderBy (a => a.name);
                } else {
                    return users.OrderByDescending (a => a.name);
                }
            }
            if ((SortDirection) filter.lvlSort != SortDirection.NONE) {
                if ((SortDirection) filter.lvlSort == SortDirection.ASC) {
                    return users.OrderBy (a => a.lvl);
                } else {
                    return users.OrderByDescending (a => a.lvl);
                }
            }
            if ((SortDirection) filter.emailSort != SortDirection.NONE) {
                if ((SortDirection) filter.emailSort == SortDirection.ASC) {
                    return users.OrderBy (a => a.email);
                } else {
                    return users.OrderByDescending (a => a.email);
                }
            }
            if ((SortDirection) filter.regDateSort != SortDirection.NONE) {
                if ((SortDirection) filter.regDateSort == SortDirection.ASC) {
                    return users.OrderBy (a => a.regDate);
                } else {
                    return users.OrderByDescending (a => a.regDate);
                }
            }
            if ((SortDirection) filter.lastActivitySort != SortDirection.NONE) {
                if ((SortDirection) filter.lastActivitySort == SortDirection.ASC) {
                    return users.OrderBy (a => a.lastOnlineDate);
                } else {
                    return users.OrderByDescending (a => a.lastOnlineDate);
                }
            }
            return users;
        }
    }
}