﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SWUserService.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SWUserService.Tasks {
    public class DailyRewardUpdater : IHostedService, IDisposable {
        private readonly ILogger _logger;
        private Timer _timer;

        public DailyRewardUpdater(ILogger<DailyRewardUpdater> logger) {
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken) {
            _logger.LogInformation("Timed Background Service DailyRewardUpdater is starting.");

            _timer = new Timer(DoWork, null, TimeSpan.FromMilliseconds(GetSleepTime()),
                TimeSpan.FromDays(1));

            return Task.CompletedTask;
        }

        private async void DoWork(object state) {
            try {
                using (var _db = new SWPlayerDbContext()) {
                    using (var tr = await _db.Database.BeginTransactionAsync()) {
                        var users = _db.Users.Include(a => a.playerInfo).Include(a => a.playerInfo.calendarRewards).ToList();
                        foreach (var user in users) {
                            var reward = user.playerInfo.calendarRewards.FirstOrDefault(a => a.id == 1);
                            if (reward == null)
                                continue;
                            reward.rewardCollected = false;
                        }
                        await _db.SaveChangesAsync();
                        tr.Commit();
                        _logger.LogInformation("Daily rewards was updated.");
                    }
                }
            } catch (Exception e) {
                _logger.LogError(e, e.Message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) {
            _logger.LogInformation("Timed Background Service DailyRewardUpdater is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose() {
            _timer?.Dispose();
        }

        private double GetSleepTime() {
            var midnightTonight = DateTime.Today.AddDays(1);
            var differenceInMilliseconds = (midnightTonight - DateTime.Now).TotalMilliseconds;
            return differenceInMilliseconds;
        }
    }
}
