﻿using Microsoft.AspNetCore.Mvc.Filters;
using SWUserService.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SWUserService.Attributes {
    public class ActivityUpdateAttribute : ActionFilterAttribute {

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) {
            var user = context.HttpContext.User;
            using (var db = new SWPlayerDbContext()) {
                using (var tr = await db.Database.BeginTransactionAsync()) {
                    var dbUser = db.Users.FirstOrDefault(a => a.UserName == user.Identity.Name);
                    if (dbUser == null) {
                        await next();
                        return;
                    }
                    dbUser.lastOnlineDate = DateTime.Now;
                    await db.SaveChangesAsync();
                    tr.Commit();
                }
            }
            await next();
        }
    }
}
