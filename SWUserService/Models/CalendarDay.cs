﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWUserService.Models {
    public class CalendarDay {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid id { get; set; }
        public int day { get; set; }
        public bool dayChecked { get; set; }
        public Guid playerInfo_id { get; set; }
        [ForeignKey("playerInfo_id")]
        public PlayerInfo playerInfo { get; set; }
    }
}
