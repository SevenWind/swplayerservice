﻿using System.Collections.Generic;

namespace SWUserService.Models {
    public class CalendarRewardExternal {
        public int id { get; set; }
        public int needCheckDaysCount { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public List<BuyItem> items { get; set; }
    }
}
