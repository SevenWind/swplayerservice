﻿namespace SWUserService.Models {
    public class ShortPlayerInfo {
        public string username { get; set; }
        public string avatar_name { get; set; }
        public Level level { get; set; }
        public int xp { get; set; }
        public PlayerCash cash { get; set; }

        public string avatarImage { get; set; }
    }
}
