﻿using System;

namespace SWUserService.Models {
    public class FilteredUser {
        public Guid id { get; set; }
        public string name { get; set; }
        public int lvl { get; set; }
        public string email { get; set; }
        public DateTime regDate { get; set; }
        public DateTime lastOnlineDate { get; set; }
        public bool isLocked { get; set; }
    }
}
