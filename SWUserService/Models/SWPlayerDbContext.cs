﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SWUserService.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using static SWUserService.Models.Enums;

namespace SWUserService.Models {
    public class SWPlayerDbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid> {
        private IConfigurationRoot configuration;

        public virtual DbSet<Level> Levels { get; set; }
        public virtual DbSet<PlayerInfo> PlayerInfos { get; set; }
        public virtual DbSet<PlayerCash> PlayerCashes { get; set; }
        public virtual DbSet<CalendarDay> CalendarDays { get; set; }
        public virtual DbSet<CalendarReward> CalendarRewards { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<InventoryItems> InventoryItems { get; set; }
        public virtual DbSet<StarFragment> StarFragments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {

            base.OnModelCreating(modelBuilder);

            modelBuilder.ForNpgsqlUseSequenceHiLo();

            modelBuilder.Entity<InventoryItems>().HasKey(a => new { a.player_id, a.item_id });

            modelBuilder.Entity<InventoryItems>()
               .HasOne(x => x.item)
               .WithMany(y => y.inventories)
               .HasForeignKey(y => y.item_id);

            modelBuilder.Entity<InventoryItems>()
                .HasOne(x => x.playerInfo)
                .WithMany(y => y.items)
                .HasForeignKey(y => y.player_id);

            modelBuilder.Entity<User>().Property(x => x.UserName).HasMaxLength(12);

            modelBuilder.Entity<CalendarDay>()
                .HasOne(s => s.playerInfo)
                .WithMany(g => g.days)
                .HasForeignKey(s => s.playerInfo_id);

            Dictionary<ItemCode, Guid> itemIds = new Dictionary<ItemCode, Guid>();
            itemIds.Add(ItemCode.Coins, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"));
            itemIds.Add(ItemCode.Gold, new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"));
            itemIds.Add(ItemCode.MagicAmulet, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"));
            itemIds.Add(ItemCode.ScrollOfHiring, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"));

            itemIds.Add(ItemCode.GekkelKilnFragment, new Guid("c4993306-2a61-4302-84a0-5825d50d1ae8"));
            itemIds.Add(ItemCode.DornKirpichFragment, new Guid("10b34f8b-defe-44f4-9dbe-3c7e375b2b1a"));
            itemIds.Add(ItemCode.CerberFragment, new Guid("7dd83f78-ad3d-49c6-a284-d5c9c68bfe01"));
            itemIds.Add(ItemCode.TorilBuiniyFragment, new Guid("e2e0c07e-e82d-4c72-8dbe-abb0ec02b86f"));
            itemIds.Add(ItemCode.LiaAlleriaFragment, new Guid("75be5201-fa5c-40b5-9e81-a5f5a09311ac"));
            itemIds.Add(ItemCode.HemingRouterFragment, new Guid("055f174a-3963-43c3-8a86-bf043b3e22d8"));

            List<Guid> rolesIds = new List<Guid>();
            rolesIds.Add(new Guid("5e4a0402-0eec-4973-a04d-aa33b83bc182"));
            rolesIds.Add(new Guid("ef2895f6-7500-46d5-993f-85abecc38780"));
            rolesIds.Add(new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"));

            var userId = new Guid("99df2e73-76d9-4994-87aa-f051c78b70d8");
            var playerInfoId = new Guid("66b2c28b-3670-46f9-ab13-89b753ee4574");


            FillLevels(modelBuilder);
            FillRoles(modelBuilder, rolesIds);
            FillPlayerInfosAndCashes(modelBuilder, playerInfoId);
            FillUsers(modelBuilder, userId, playerInfoId, rolesIds[1]);

            FillRarity(modelBuilder);
            FillItems(modelBuilder, itemIds);
            FillCalendarDays(modelBuilder, playerInfoId);
            FillCalendarRewards(modelBuilder, playerInfoId);

            FillStarFragments(modelBuilder);
            //FillPlayerCollection(modelBuilder, playerInfoId);
        }

        private void FillStarFragments(ModelBuilder modelBuilder) {
            modelBuilder.Entity<StarFragment>().HasData(
                new StarFragment() {
                    starCount = 1,
                    needFragments = 10,
                    borderColor = "#00FF7F"
               },
               new StarFragment() {
                   starCount = 2,
                   needFragments = 30,
                   borderColor = "#40E0D0"
               },
               new StarFragment() {
                   starCount = 3,
                   needFragments = 80,
                   borderColor = "#8A2BE2"
               },
               new StarFragment() {
                   starCount = 4,
                   needFragments = 150,
                   borderColor = "#B8860B"
               },
               new StarFragment() {
                   starCount = 5,
                   needFragments = 300,
                   borderColor = "#8B0000"
               }
           );
        }

        private void FillPlayerCollection(ModelBuilder modelBuilder, Guid playerInfoId) {
            modelBuilder.Entity<PlayerCollectionItem>().HasData(new PlayerCollectionItem {
                id = 1,
                character_id = 1,
                fragmentCount = 7,
                starCount = 0,
                playerInfo_id = playerInfoId
            });
        }

        private void FillPlayerInfosAndCashes(ModelBuilder modelBuilder, Guid playerInfoId) {
            var cashId = Guid.NewGuid();
            modelBuilder.Entity<PlayerCash>().HasData(
                new PlayerCash() {
                    id = cashId,
                    coins = 12114,
                    gold = 100
                }
            );

            modelBuilder.Entity<PlayerInfo>().HasData(
                new PlayerInfo() {
                    id = playerInfoId,
                    level_id = 13,
                    xp = 13657,
                    cash_id = cashId,
                }
            );
        }

        private void FillUsers(ModelBuilder modelBuilder, Guid userId, Guid playerInfoId, Guid roleId) {
            var hasher = new PasswordHasher<User>();
            modelBuilder.Entity<User>().HasData(new User {
                Id = userId,
                UserName = "Guron",
                NormalizedUserName = "GURON",
                Email = "guron@mail.ru",
                NormalizedEmail = "GURON@MAIL.RU",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "q1w@e3R4"),
                SecurityStamp = string.Empty,
                avatarName = "guron.png",
                birthDate = new DateTime(1995, 5, 19),
                lastOnlineDate = DateTime.Now.AddHours(-1),
                registryDate = DateTime.Now.AddDays(-1),
                playerInfo_id = playerInfoId
            });

            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>() {
                    UserId = userId,
                    RoleId = roleId
                }
            );
        }

        private void FillCalendarRewards(ModelBuilder modelBuilder, Guid playerInfoId) {
            modelBuilder.Entity<CalendarReward>().HasData(
               new CalendarReward() {
                   id = 1,
                   code = "DAILY",
                   playerInfo_id = playerInfoId,
               },
               new CalendarReward() {
                   id = 2,
                   code = "7_DAYS",
                   playerInfo_id = playerInfoId,
               },
               new CalendarReward() {
                   id = 3,
                   code = "14_DAYS",
                   playerInfo_id = playerInfoId,
               },
               new CalendarReward() {
                   id = 4,
                   code = "21_DAYS",
                   playerInfo_id = playerInfoId,
               },
               new CalendarReward() {
                   id = 5,
                   code = "FULL_MONTH",
                   playerInfo_id = playerInfoId,
               }
            );

        }

        private void FillCalendarDays(ModelBuilder modelBuilder, Guid playerInfoId) {
            modelBuilder.Entity<CalendarDay>().HasData(
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 1,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 2,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 3,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 4,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 5,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 6,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 7,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 8,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 9,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 10,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 11,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 12,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 13,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 14,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 15,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 16,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 17,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 18,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 19,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 20,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 21,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 22,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 23,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 24,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 25,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 26,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 27,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 28,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 29,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 30,
                    playerInfo_id = playerInfoId,
                },
                new CalendarDay() {
                    id = Guid.NewGuid(),
                    day = 31,
                    playerInfo_id = playerInfoId,
                }
            );
        }

        private void FillRoles(ModelBuilder modelBuilder, List<Guid> rolesIds) {
            modelBuilder.Entity<IdentityRole<Guid>>().HasData(
                new IdentityRole<Guid>() {
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    Id = rolesIds[0],
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole<Guid>() {
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    Id = rolesIds[1],
                    Name = "Player",
                    NormalizedName = "PLAYER"
                },
                new IdentityRole<Guid>() {
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    Id = rolesIds[2],
                    Name = "Tester",
                    NormalizedName = "TESTER"
                }
            );
        }

        private void FillItems(ModelBuilder modelBuilder, Dictionary<ItemCode, Guid> itemIds) {
            modelBuilder.Entity<Item>().HasData(
                new Item() {
                    id = itemIds[ItemCode.Coins],
                    name = "Монеты",
                    code = ItemCode.Coins.ToString(),
                    rarity_id = 2,
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "coin.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.Gold],
                    name = "Золото",
                    rarity_id = 4,
                    code = ItemCode.Gold.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "gold.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.ScrollOfHiring],
                    name = "Свиток Найма",
                    rarity_id = 1,
                    code = ItemCode.ScrollOfHiring.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "scroll.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.MagicAmulet],
                    name = "Магический Амулет",
                    rarity_id = 1,
                    code = ItemCode.MagicAmulet.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "amulet.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.GekkelKilnFragment],
                    name = "Фрагмент \"Геккель Килн\"",
                    rarity_id = 4,
                    characterId = 1,
                    code = "Character_" + ItemCode.GekkelKilnFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "gekkel_kiln.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.DornKirpichFragment],
                    name = "Фрагмент \"Дорн Кирпич\"",
                    rarity_id = 4,
                    characterId = 2,
                    code = "Character_" + ItemCode.DornKirpichFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "dorn_kirpich.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.CerberFragment],
                    name = "Фрагмент \"Цербер\"",
                    rarity_id = 4,
                    characterId = 3,
                    code = "Character_" + ItemCode.CerberFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Cerber.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.TorilBuiniyFragment],
                    name = "Фрагмент \"Торил Буйный\"",
                    rarity_id = 4,
                    characterId = 4,
                    code = "Character_" + ItemCode.TorilBuiniyFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Toril_buiniy.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.LiaAlleriaFragment],
                    name = "Фрагмент \"Лия Аллерия\"",
                    rarity_id = 4,
                    characterId = 5,
                    code = "Character_" + ItemCode.LiaAlleriaFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Lia_alleria.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.HemingRouterFragment],
                    name = "Фрагмент \"Хеминг Роутер\"",
                    rarity_id = 4,
                    characterId = 6,
                    code = "Character_" + ItemCode.HemingRouterFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "hemming_router.png"))))),
                }
            );
        }

        private void FillRarity(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Rarity>().HasData(
                new Rarity() {
                    id = 1,
                    code = "None",
                },
                new Rarity() {
                    id = 2,
                    code = "COMMON",
                },
                new Rarity() {
                    id = 3,
                    code = "UNCOMMON",
                },
                new Rarity() {
                    id = 4,
                    code = "RARE",
                }
            );
        }

        private void FillLevels(ModelBuilder modelBuilder) {
            var levels = new List<Level>();
            var maxLvl = configuration.GetValue<int>("MaxLevel");

            int xpTo2Lvl = configuration.GetValue<int>("xpTo2Lvl");
            double nextLvlXpCoefficient = configuration.GetValue<double>("nextLvlXpCoefficient");

            for (int i = 1; i <= maxLvl; i++) {
                levels.Add(new Level() { level = i, needXpForNext = xpTo2Lvl + i * (int)Math.Floor(xpTo2Lvl * nextLvlXpCoefficient) });
            }

            modelBuilder.Entity<Level>().HasData(levels.ToArray());
        }
    }
}
