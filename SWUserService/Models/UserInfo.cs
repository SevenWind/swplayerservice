﻿using System.IO;

namespace SWUserService.Models {
    public class UserInfo {
        public string name { get; set; }
        public int level { get; set; }
        public int currXp { get; set; }
        public int nextXp { get; set; }
        public string email { get; set; }
        public string avatar_path { get; set; }
        public string avatar_name { get; set; }
        public string password { get; set; }
        public string birthday { get; set; }
        public FileInfo avatarImageFile { get; set; }
    }
}
