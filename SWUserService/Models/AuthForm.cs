﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWUserService.Models {
    public class AuthForm {
        public string email { get; set; }
        public string password { get; set; }
        public bool rememberMe { get; set; }
    }
}
