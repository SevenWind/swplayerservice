﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWUserService.Models {
    public class DisplayUserProfile {
        public string birthday { get; set; }
        public string avatar_path { get; set; }
        public string avatar_name { get; set; }
        public string avatarImage { get; set; }

        public string name { get; set; }
        public int level { get; set; }
        public int xp { get; set; }
        public int nextXp { get; set; }
        public string email { get; set; }

        public string current_password { get; set; }
        public string password { get; set; }
        public string confirm_password { get; set; }

        
    }
}
