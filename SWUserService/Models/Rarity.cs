﻿using System.ComponentModel.DataAnnotations;

namespace SWUserService.Models {
    public class Rarity {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
    }
}