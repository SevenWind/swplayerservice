﻿using System;

namespace SWUserService.Models {
    public class RegForm {
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string confirmPassword { get; set; }
        public DateTime birthdate { get; set; }
    }
}
