﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWUserService.Models {
    public class PlayerInfo {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid id { get; set; }
        public int xp { get; set; }
        public Guid cash_id { get; set; }
        [ForeignKey("cash_id")]
        public PlayerCash cash { get; set; }
        public int level_id { get; set; }
        [ForeignKey("level_id")]
        public Level level { get; set; }
  
        public List<InventoryItems> items { get; set; }
        public List<CalendarDay> days { get; set; }
        public List<CalendarReward> calendarRewards { get; set; }
        public List<PlayerCollectionItem> collection { get; set; }
    }
}
