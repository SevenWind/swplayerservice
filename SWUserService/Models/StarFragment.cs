﻿using System.ComponentModel.DataAnnotations;

namespace SWUserService.Models {
    public class StarFragment {
        [Key]
        public int starCount { get; set; }
        public int needFragments { get; set; }
        public string borderColor { get; set; }
    }
}
