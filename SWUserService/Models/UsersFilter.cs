﻿using static SWUserService.Models.Enums;

namespace SWUserService.Models {
    public class UsersFilter {
        public string name { get; set; }
        public int fromLvl { get; set; }
        public int toLvl { get; set; }
        public string email { get; set; }
        public string fromRegDate { get; set; }
        public string toRegDate { get; set; }
        public int status { get; set; }

        public int nameSort { get; set; }
        public int lvlSort { get; set; }
        public int emailSort { get; set; }
        public int regDateSort { get; set; }
        public int lastActivitySort { get; set; }
    }
}
