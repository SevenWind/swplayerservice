﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWUserService.Models {
    public class User : IdentityUser<Guid> {
        [Required(ErrorMessage = "Дата рождения не указана")]
        public DateTime birthDate { get; set; }
        public DateTime registryDate { get; set; }
        public DateTime lastOnlineDate { get; set; }
        
        public Guid playerInfo_id { get; set; }
        [ForeignKey("playerInfo_id")]
        public PlayerInfo playerInfo { get; set; }

        public string avatarName { get; set; }
        public string avatarImage { get; set; }
    }
}
