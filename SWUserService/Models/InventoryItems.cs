﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWUserService.Models {
    public class InventoryItems {
        public Guid player_id { get; set; }
        [ForeignKey("player_id")]
        public PlayerInfo playerInfo { get; set; }

        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }

        public int count { get; set; }
    }
}
