﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWUserService.Models {
    public class Level {
        [Key]
        public int level { get; set; }
        public int needXpForNext { get; set; }
    }
}
