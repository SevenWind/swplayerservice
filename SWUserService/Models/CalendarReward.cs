﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWUserService.Models {
    public class CalendarReward {
        [Key]
        public int id { get; set; }
        public bool rewardCollected { get; set; }
        public string code { get; set; }

        public Guid playerInfo_id { get; set; }
        [ForeignKey("playerInfo_id")]
        public PlayerInfo playerInfo { get; set; }
    }
}
