﻿namespace SWUserService.Models {
    public class Enums {
        public enum ItemCode {
            Coins = 1,
            Gold,
            ScrollOfHiring,
            MagicAmulet,
            GekkelKilnFragment,
            DornKirpichFragment,
            CerberFragment,
            TorilBuiniyFragment,
            LiaAlleriaFragment,
            HemingRouterFragment,
        }

        public static class AuthRole {
            public const string ADMIN = "Admin";
            public const string PLAYER = "Player";
            public const string TESTER = "Tester";
            public const string ANY = "Admin, Player, Tester";
        }

        public static class ItemPrefix {
            public const string CHARACTER_PREFIX = "Character_";
        }

        public enum UserStatus {
            Все = -1,
            В_сети = 0,
            Недавно_был_в_сети,
            Не_в_сети,
            Заблокирован
        }

        public enum SortDirection {
            DESC = -1,
            NONE = 0,
            ASC = 1
        }

        public enum TimeType {
            MIN = 0,
            HOUR,
            DAY,
            MONTH
        }
    }
}
