﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWUserService.Models {
    public class BuyItem {
        public Guid itemId { get; set; }
        public string currency { get; set; }
        public int count { get; set; }
        public int cost { get; set; }
        public int xp { get; set; }
    }
}
